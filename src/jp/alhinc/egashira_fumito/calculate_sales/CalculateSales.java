package jp.alhinc.egashira_fumito.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) throws IOException {
		//支店定義ファイルの呼び出し
		Map<String, String> branchMap = new HashMap<>();
		Map<String, Long > salesMap = new HashMap<String, Long>();

		BufferedReader br = null;
		FileReader fr = null;
		try {
			File file = new File(args[0], "branch.lst");
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String branchLine;
			String[] splitedCodeName ;

			while((branchLine = br.readLine()) != null) {
				if(!(branchLine.matches("^[0-9]{3},[^,]+" ))) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}
				splitedCodeName = branchLine.split(",");		  //，で区切って分けた
				branchMap.put(splitedCodeName[0], splitedCodeName[1]); //配列[0]に支店コード[1]に支店名
				salesMap.put(splitedCodeName[0], (long) 0);
			}
		}catch(FileNotFoundException e) { //ファイルを読む→そもそもファイルがない場合ここに来る
			System.out.println("支店定義ファイルが存在しません");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
			}
		}
		//rcdかつ8桁の売り上げの読み込み
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String name){
				if (name.matches("[0-9]{8}\\.(rcd)$") && new File(file, name).isFile()){
					return true;
				}else{
					return false;
				}
			}
		};
		File[] files = new File(args[0]).listFiles(filter);
		int checkNum = Integer.parseInt(files[0].getName().replace(".rcd","" ));
		for(int serialNum = 0 ; serialNum < files.length ; serialNum++) {
			if(Integer.parseInt(files[serialNum].getName().replace(".rcd","" )) != checkNum + serialNum){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int fileNum = 0 ; fileNum < files.length ; fileNum++) { //fileNumがファイルの個数まで
			br = null;
			try {
				fr = new FileReader(files[fileNum]);
				br = new BufferedReader(fr);
				String counter;
				int lineCount = 0;
				while((counter = br.readLine()) != null) {
					lineCount++;
					if(lineCount > 2) {
	                	System.out.println((files[fileNum]).getName() + "のフォーマットが不正です");
	                	return;
	                }
					if(branchMap.containsKey(counter)) {
						salesMap.put(counter, salesMap.get(counter) + Long.parseLong(br.readLine()));
						lineCount++;
						System.out.println(salesMap.keySet());
					}else{
						System.out.println(files[fileNum].getName()+ "の支店コードが不正です");
						return;
					}
					if(10 <= String.valueOf(salesMap.get(counter)).length()){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					}catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
						return;
					}
				}
			}
		}
		//ファイル作成
		BufferedWriter bw = null;
		try {
			File file = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
				for(String line : salesMap.keySet()) {
					bw.write(line + "," +branchMap.get(line) +","+salesMap.get(line));
					bw.newLine();
					//System.out.println(line + "," +branchMap.get(line) +","+salesMap.get(line));
				}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
					return;
				}
			}
		}
	}
}